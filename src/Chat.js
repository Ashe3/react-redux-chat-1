import './App.css';

import { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import Header from './components/Header';
import MsgList from './components/Messages';
import SendMessage from './components/SendMessage';
import Spinner from './components/Spinner';
import Editor from './components/Editor';

import { getMessageList } from './helpers/apiHelper';
import { addMessages } from './store/actionCreators/messageActionCreators';
import { getParticipants, last } from './helpers/arrayHandleHelper';


function Chat({ get, isShowModal, messageCount, participants, lastMessage }) {
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {

    getMessageList()
      .then(messages => get(messages))

    setLoading(false);
  }, []);

  const loaded = () => {
    return (
      <div>
        <div className='App'>
          <Header participants={participants} messages={messageCount} lastMsg={lastMessage} />
          <MsgList />
          <SendMessage />
        </div>
        {isShowModal ? <Editor /> : null}
      </div>
    );
  }

  return (
    <div>
      {isLoading ? <Spinner /> : loaded()}
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages,
    isShowModal: state.modal.isShow,
    messageCount: state.messages.length,
    participants: getParticipants(state.messages),
    lastMessage: last(state.messages)
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    get: (messages) => dispatch(addMessages(messages))
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Chat);

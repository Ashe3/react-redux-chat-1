import React from 'react';
import { connect } from 'react-redux';

import MsgBox from './Message';
import UserMessage from './UserMessage';
import { removeMessage } from '../store/actionCreators/messageActionCreators';
import { findMessagePosition, sortMessagesByDate } from '../helpers/arrayHandleHelper';


class MsgList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };

    this.rmMessage = this.rmMessage.bind(this);
  }

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  }

  componentDidUpdate(prev) {
    this.scrollToBottom();
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  rmMessage(id) {
    const messageIdx = findMessagePosition(id, this.props.messages);
    const newMessageList = [...this.props.messages];

    if (messageIdx > 0) {
      newMessageList.splice(messageIdx, 1);
    }
    this.props.remove(newMessageList);
  }

  render() {

    const messageList = sortMessagesByDate(this.props.messages).map(({ id, userId, avatar, user, text, createdAt, editedAt }, idx) => {
      switch (userId) {
        case '7':
          return <UserMessage
            key={id + idx}
            id={id}
            idx={idx}
            userId={userId}
            text={text}
            createdAt={createdAt}
            editedAt={editedAt}
            handleRemove={this.rmMessage}
            handleEdit={this.props.handleEdit} />
        default:
          return <MsgBox
            key={id + idx}
            id={id}
            avatar={avatar}
            userId={userId}
            username={user}
            text={text}
            createdAt={createdAt}
            editedAt={editedAt} />
      }
    });

    return (
      <div className='message-list'>
        {messageList}
        <div style={{ float: "left", clear: "both", opacity: 0 }}
          ref={(el) => { this.messagesEnd = el; }}>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    remove: (messages) => dispatch(removeMessage(messages))
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(MsgList);
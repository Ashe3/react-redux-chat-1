import React from 'react';
import { connect } from 'react-redux';

import { showModal } from '../store/actionCreators/modalActionCreators';

const UserMessage = (props) => {

  const normalizeDate = (date) => {
    return new Date(date).toLocaleString('ru-RU');
  }

  return (
    <div className="message user-message">
      <div className='container'>
        <div className='edit'>
          <span className="material-icons" onClick={() => { props.show(props) }}>
            create
        </span>
          <span className="material-icons" onClick={() => { props.handleRemove(props.id) }}>
            delete
        </span>
        </div>
        <div className='text'>
          <span>{props.text}</span>
          <span className='date'>{props.editedAt ? `Edited ${normalizeDate(props.editedAt)}` : normalizeDate(props.createdAt)}</span>
        </div>
      </div>
    </div>
  )
}

const mapDispatchToProps = (dispatch) => {
  return {
    show: (message) => dispatch(showModal(message))
  };
};

export default connect(null, mapDispatchToProps)(UserMessage);
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { hideModal } from '../store/actionCreators/modalActionCreators';
import { updateMessage } from '../store/actionCreators/messageActionCreators';
import { findMessagePosition } from '../helpers/arrayHandleHelper';

const Editor = ({ close, message, messages, save }) => {
  const [messageText, setMessage] = useState(message.text);

  const handleChange = (e) => {
    setMessage(e.target.value);
  };

  const handleSave = () => {
    const messageList = [...messages];
    const messageIdx = findMessagePosition(message.id, messageList);
    messageList[messageIdx] = editMessageData();
    save(messageList);
    close()
  }

  const editMessageData = () => {
    return {
      ...message,
      text: messageText,
      editedAt: new Date(Date.now())
    };
  };

  return (
    <div id="modal" className="modal">
      <div className="modal-content">
        <span className="close" onClick={close}>&times;</span>
        <span>Edit message</span>
        <textarea rows='15' cols='30' autoFocus value={messageText} onChange={handleChange}></textarea>
        <div>
          <div onClick={handleSave}>Save</div>
          <div onClick={close}>Cancel</div>
        </div>
      </div>
    </div>
  )
};

const mapStateToProps = (state) => {
  return {
    message: state.modal.message,
    messages: state.messages
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    close: () => dispatch(hideModal()),
    save: (messages) => dispatch(updateMessage(messages))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Editor);
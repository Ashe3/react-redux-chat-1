import React from 'react';

import logo from '../img/logo.svg';

const Spinner = () => {
  return (
    <div className='center'>
      <img src={logo} className="load-logo" alt='logo'/>
    </div>
  )
}

export default Spinner;
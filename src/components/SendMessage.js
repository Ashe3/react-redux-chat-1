import React from 'react';
import { connect } from 'react-redux';
import { addMessage } from '../store/actionCreators/messageActionCreators';
import { showModal } from '../store/actionCreators/modalActionCreators';

class SendMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      lastMessage: {}
    }
    this.onChange = this.onChange.bind(this);
    this.onSend = this.onSend.bind(this);
    this.handlePressButton = this.handlePressButton.bind(this);
  }

  onChange(event) {
    const { value } = event.target;
    this.setState({
      value
    });
  }

  onSend() {
    const newMessage = this.getMessageDetails();
    if (newMessage.text.length !== 0) {
      this.props.addNewMessage(newMessage);
      this.setState({ value: '', lastMessage: newMessage });
    }
  }

  handlePressButton(event) {
    if (event.type === 'keyup') {
      if (event.key === 'Enter') {  
          this.onSend();
      }

      if (event.key === 'ArrowUp') {
        this.props.show(this.state.lastMessage);
      }
    }
  }

  getMessageDetails() {
    return {
      id: Math.floor(Math.random() * Math.floor(100)),
      userId: '7',
      user: 'current user',
      text: this.state.value,
      createdAt: new Date(Date.now()),
      editedAt: ''
    }
  }

  render() {
    return (
      <div className='send' onKeyPress={this.handlePressButton}>
        <input 
        type='text' 
        className='send-field' 
        value={this.state.value} 
        onChange={this.onChange} 
        onKeyUp={this.handlePressButton} 
        placeholder='message here or click ^ to edit'/>
        <div className='sendBtn center' onClick={this.onSend}>Send</div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNewMessage: (message) => dispatch(addMessage(message)),
    show: (message) => dispatch(showModal(message))
  }
};

export default connect(null, mapDispatchToProps)(SendMessage);
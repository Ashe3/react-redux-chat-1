const apiHelper = (url, endpoint, method) => {
  const URL = url + endpoint;
  const options = {
    method
  };

  return fetch(URL, options)
    .then(response => response.json())
    .then(response => response)
    .catch(err => console.log(err));
};

export const getMessageList = async () => {
  const url = 'https://edikdolynskyi.github.io';
  const endpoint = '/react_sources/messages.json';
  const method = 'GET';

  return await apiHelper(url, endpoint, method);
};
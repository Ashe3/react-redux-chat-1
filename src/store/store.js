import { combineReducers, createStore } from 'redux';

import initialState from './initialState';
import { messageReducer as messages } from './reducers/messageReducer';
import { modalReducer as modal } from './reducers/modalReducer';

const reducers = combineReducers({modal, messages})

const store = createStore(reducers, initialState);

export default store;
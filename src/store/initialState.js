const initialState = {
  messages: [],
  modal: {
    isShow: false,
    message: {}
  }
};

export default initialState;
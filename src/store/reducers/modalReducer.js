import { HIDE_MODAL, SHOW_MODAL } from "../actions/modalActions";

export const modalReducer = (state = {}, action) => {
  switch (action.type) {
    case SHOW_MODAL: return action.value;
    case HIDE_MODAL: return action.value;

    default: return state;
  }
}
import { ADD_MESSAGE, ADD_MESSAGES, REMOVE_MESSAGE, UPDATE_MESSAGE } from '../actions/messageActions';

export const messageReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_MESSAGE: return [...state, action.value];
    case ADD_MESSAGES: return action.value;
    case REMOVE_MESSAGE: return action.value;
    case UPDATE_MESSAGE: return action.value;

    default: return state;
  }
}
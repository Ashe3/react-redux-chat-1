import { HIDE_MODAL, SHOW_MODAL } from "../actions/modalActions"

export const showModal = (message) => {
  return {
    type: SHOW_MODAL,
    value: {
      isShow: true,
      message
    }
  };
};

export const hideModal = () => {
  return {
    type: HIDE_MODAL,
    value: {
      isShow: false,
      message: ''
    }
  };
};
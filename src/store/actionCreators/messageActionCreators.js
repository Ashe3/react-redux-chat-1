import { ADD_MESSAGE, ADD_MESSAGES, REMOVE_MESSAGE, UPDATE_MESSAGE } from '../actions/messageActions'

export const addMessage = (message) => {
  return {
    type: ADD_MESSAGE,
    value: message
  };
};

export const addMessages = (messages) => {
  return {
    type: ADD_MESSAGES,
    value: messages
  };
};

export const removeMessage = (messages) => {
  return {
    type: REMOVE_MESSAGE,
    value: messages
  };
};

export const updateMessage = (messages) => {
  return {
    type: UPDATE_MESSAGE,
    value: messages
  }
}